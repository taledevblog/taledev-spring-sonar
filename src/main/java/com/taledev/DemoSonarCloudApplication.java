package com.taledev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSonarCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSonarCloudApplication.class, args);
	}

}
